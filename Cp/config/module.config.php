<?php
return array(
    'router' => array(
        'routes' => array(
            'cp' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/Cp',
                    'defaults' => array(
                        'controller' => 'Cp\Controller\Index',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'statistic' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/statistic[/:range]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Index',
                                'action' => 'index'
                            )
                        )
                    ),
                    'user' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/user[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\User',
                                'action' => 'index'
                            )
                        )
                    ),
                    'coupon' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/coupon[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Coupon',
                                'action' => 'index'
                            )
                        )
                    ),
                    'ad' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/ad[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Ad',
                                'action' => 'index'
                            )
                        )
                    ),
                    'news' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/news[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\News',
                                'action' => 'index'
                            )
                        )
                    ),
                    'faq' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/faq[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Faq',
                                'action' => 'index'
                            )
                        )
                    ),
                    'setting' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/setting[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Setting',
                                'action' => 'index'
                            )
                        )
                    ),
                    'subscription' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/subscription[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Subscription',
                                'action' => 'index'
                            )
                        )
                    ),
                    'activity' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/activity[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Activity',
                                'action' => 'index'
                            )
                        )
                    ),
                    'component' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/component[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Component',
                                'action' => 'index'
                            )
                        )
                    ),
                    'wantedcomponent' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/post[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Wantedcomponent',
                                'action' => 'index'
                            )
                        )
                    ),
                    'groups' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/groups[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Groups',
                                'action' => 'index'
                            )
                        )
                    ),
                    'orders' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/orders[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Orders',
                                'action' => 'index'
                            )
                        )
                    ),
                    'static' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/static[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Static',
                                'action' => 'index'
                            )
                        )
                    ),
                    'vat' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/vat[/:action]',
                            'defaults' => array(
                                'controller' => 'Cp\Controller\Vat',
                                'action' => 'index'
                            )
                        )
                    ),
                )
            )
            
        )
    ),
    'service_manager' => array(
        'invokables' => array(
            'CouponForm' => 'App\Form\Coupon',
            'NewsForm' => 'App\Form\News',
            'AdForm' => 'App\Form\Ad',
            'SettingForm' => 'App\Form\Setting',
            'SubscriptionForm' => 'App\Form\Subscription',
            'App\Form\SellerGroup' => 'App\Form\SellerGroup'
        ),
        'factories' => array(
            'CpNavigation' => 'Cp\Navigation\Service\CpNavigationFactory'
        ),
        'abstract_factories' => array(),
        'aliases' => array()
    ),
    'controllers' => array(
        'invokables' => array(
            'Cp\Controller\Index' => 'Cp\Controller\IndexController',
            'Cp\Controller\User' => 'Cp\Controller\UserController',
            'Cp\Controller\Coupon' => 'Cp\Controller\CouponController',
            'Cp\Controller\News' => 'Cp\Controller\NewsController',
            'Cp\Controller\Activity' => 'Cp\Controller\ActivityController',
            'Cp\Controller\Ad' => 'Cp\Controller\AdController',
            'Cp\Controller\Faq' => 'Cp\Controller\FaqController',
            'Cp\Controller\Setting' => 'Cp\Controller\SettingController',
            'Cp\Controller\Subscription' => 'Cp\Controller\SubscriptionController',
            'Cp\Controller\Component' => 'Cp\Controller\ComponentController',
            'Cp\Controller\Wantedcomponent' => 'Cp\Controller\WantedcomponentController',
            'Cp\Controller\Groups' => 'Cp\Controller\GroupsController',
            'Cp\Controller\Orders' => 'Cp\Controller\OrdersController',
            'Cp\Controller\Static' => 'Cp\Controller\StaticController',
            'Cp\Controller\Vat' => 'Cp\Controller\VatController',
        )
    ),
    'controller_plugins' => array(
        'invokables' => array()
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'cp/layout' => __DIR__ . '/../view/layout/layout.phtml'
        ),
        'template_path_stack' => array(
            'cp' => __DIR__ . '/../view'
        )
    ),
    'view_helpers' => array(
        'invokables' => array()
    )
    ,
    'navigation' => array(
        'cp' => array(
            array(
                'label' => _('Ads/Posts Management'),
                'action' => 'index',
                'route' => 'cp/component'
            ),
            array(
                'label' => _('User Management'),
                'action' => 'index',
                'route' => 'cp/user'
            ),
            array(
                'label' => _('Ads Management'),
                'action' => 'index',
                'route' => 'cp/ad'
            ),
            array(
                'label' => _('Coupons Management'),
                'action' => 'index',
                'route' => 'cp/coupon'
            ),
            array(
                'label' => _('Subscriptions Management'),
                'action' => 'index',
                'route' => 'cp/subscription'
            ),
           /*  array(
                'label' => _('Security Announcements Management'),
                'action' => 'index',
                'route' => 'cp/news'
            ), */
            array(
                'label' => _('Static Pages Management'),
                'action' => 'index',
                'route' => 'cp/static'
            ),
            array(
                'label' => _('FAQ Management'),
                'action' => 'index',
                'route' => 'cp/faq'
            ),
            array(
                'label' => _('System Settings'),
                'action' => 'index',
                'route' => 'cp/setting'
            ),
            array(
                'label' => _('Customer activity'),
                'action' => 'index',
                'route' => 'cp/activity'
            ),
            array(
                'label' => _('Groups of Sellers'),
                'action' => 'index',
                'route' => 'cp/groups'
            ),
            array(
                'label' => _('Payment Log'),
                'action' => 'index',
                'route' => 'cp/orders'
            ),
            array(
                'label' => _('VAT'),
                'action' => 'index',
                'route' => 'cp/vat'
            )
        )
    )
);
