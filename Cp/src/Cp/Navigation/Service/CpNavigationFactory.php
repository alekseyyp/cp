<?php
namespace Cp\Navigation\Service;

use Zend\Navigation\Service\AbstractNavigationFactory;
/**
 * Custom navigation factory.
 */
class CpNavigationFactory extends AbstractNavigationFactory
{
	/**
	 * @return string
	 */
	protected function getName()
	{
		return 'cp';
	}
}
