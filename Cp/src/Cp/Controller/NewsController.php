<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\News as NewsModel;

class NewsController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->script()->addJs("/ckeditor/ckeditor.js");
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addNamespace("CustomLib.editor.Ckeditor")
    			->addJs("/js/cp/news-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\News')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("id", "title", "date_published", "text"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$form = $sl->get("NewsForm");
    	$mapper = $sl->get("App\Mapper\News");
    	
    	$data = $request->getPost();
    	
    	$form->setData($data);
    	$form->setInputFilter($mapper->getInputFilter());
    	
    	$responseData = array();
    	
    	if($form->isValid())
    	{
    		$validData = $form->getData();
    		if($validData['id'] > 0)
    		{
    			$news = $mapper->fetchOne($validData['id']);
    		}
    		else
    		{
    			$news = new NewsModel();
    		}	 
    		$news = $mapper->hydrate($validData, $news);
    		
    		
    		$mapper->save($news);
    		
    	}	
    	else{
    		$msg = array();
    		$messages = $form->getMessages();
    		foreach($messages as $el)
    		{
    			foreach($el as $message)
    			{
    				$msg[] = $message;
    			}
    		}
    		$responseData['success'] = false;
    		$responseData['msg'] = implode("<br>", $msg);
    		
    	}
    	
    	return $this->response($responseData);
    }
    
    public function publishAction(){
    	$id = $this->getRequest()->getPost("id");
    	$newsMapper = $this->getServiceLocator()->get("App\Mapper\News");
    	$news = $newsMapper->fetchOne($id);
    	$news->setIsPublished(1);
    	$newsMapper->save($news, array("is_published"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("Security Announcement #%s has been successfully published."), $id)
    	));
    }
    
 	public function deleteAction(){
    	$id = $this->getRequest()->getPost("id");
    	$newsMapper = $this->getServiceLocator()->get("App\Mapper\News");
    	$newsMapper->delete($id);
    	
    	return $this->response(array(
    		'message' => sprintf(_("Security Announcement #%s has been successfully deleted."), $id)
    	));
    }
    
    public function unpublishAction(){
    	$id = $this->getRequest()->getPost("id");
    	$newsMapper = $this->getServiceLocator()->get("App\Mapper\News");
    	$news = $newsMapper->fetchOne($id);
    	$news->setIsPublished(0);
    	$newsMapper->save($news, array("is_published"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("Security Announcement #%s has been successfully unpublished."), $id)
    	));
    }
}
