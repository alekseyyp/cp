<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;


class ActivityController extends AbstractActionController
{
    public function indexAction()
    {
    	$defaultEmail = $this->getRequest()->getQuery("email", "");
    	
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addJsCode('var defaultFilter = "' . $defaultEmail . '";')
    			->addJs("/js/cp/activity-list.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\UserActivity')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("type", "user_activities.name", "action_date", "u.email"))
    			->getData();
    	 //var_dump($data);
    	return $this->response($data);
    }
}
