<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\Coupon as CouponModel;

class CouponController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addJs("/js/cp/coupons-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\Coupon')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("id", "code"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$form = $sl->get("CouponForm");
    	$mapper = $sl->get("App\Mapper\Coupon");
    	
    	$data = $request->getPost();
    	
    	$form->setData($data);
    	$form->setInputFilter($mapper->getInputFilter());
    	
    	$responseData = array();
    	
    	if($form->isValid())
    	{
    		$validData = $form->getData();
    		if($validData['id'] > 0)
    		{
    			$couponModel = $mapper->fetchOne($validData['id']);
    			$couponModel = $mapper->hydrate($validData, $couponModel);
    		}
    		else
    		{
    			$couponModel = new CouponModel();
    			$couponModel = $mapper->hydrate($validData, $couponModel);
    			$couponModel->setIsActive(1);
    		}	 
    		
    		
    		$mapper->save($couponModel);
    		
    	}	
    	else{
    		$msg = array();
    		$messages = $form->getMessages();
    		foreach($messages as $el)
    		{
    			foreach($el as $message)
    			{
    				$msg[] = $message;
    			}
    		}
    		$responseData['success'] = false;
    		$responseData['msg'] = implode("<br>", $msg);
    		
    	}
    	
    	return $this->response($responseData);
    }
    
    public function activateAction(){
    	$id = $this->getRequest()->getPost("id");
    	$couponMapper = $this->getServiceLocator()->get("App\Mapper\Coupon");
    	$coupon = $couponMapper->fetchOne($id);
    	$coupon->setIsActive(1);
    	$couponMapper->save($coupon, array("is_active"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("Coupon #%s has been successfully activated."), $id)
    	));
    }
    
    public function blockAction(){
    	$id = $this->getRequest()->getPost("id");
    	$couponMapper = $this->getServiceLocator()->get("App\Mapper\Coupon");
    	$coupon = $couponMapper->fetchOne($id);
    	$coupon->setIsActive(0);
    	$couponMapper->save($coupon, array("is_active"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("Coupon #%s has been successfully blocked."), $id)
    	));
    }

    public function removeAction()
    {
        $id = $this->getRequest()->getPost("id");
        $couponMapper = $this->getServiceLocator()->get("App\Mapper\Coupon");
        $coupon = $couponMapper->fetchOne($id);
        $couponMapper->delete($coupon->getId());

        return $this->response(array(
            'message' => sprintf(_("Coupon #%s has been successfully removed."), $id)
        ));
    }
}
