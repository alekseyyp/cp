<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\News as NewsModel;

class VatController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->script()->addJs("/ckeditor/ckeditor.js");
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addNamespace("CustomLib.editor.Ckeditor")
    			->addJs("/js/cp/vat-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\Country')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("id", "title", "internet", "vat"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();

    	$mapper = $sl->get('App\Mapper\Country');
    	
    	$data = $request->getPost();

        if($data['id'] > 0)
        {
            $model = $mapper->fetchOne($data['id']);
            $model->setVat($data['vat']);
            $mapper->save($model);
        }
        $responseData = array();

        $responseData['success'] = true;
        $responseData['msg'] = 'VAT updated successfully';
    	
    	return $this->response($responseData);
    }
}