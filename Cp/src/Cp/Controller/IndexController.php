<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;


class IndexController extends AbstractActionController
{
    public function indexAction()
    {

        $selected = $this->getEvent()->getRouteMatch()->getParam('range', 1);

        $statisticMapper = $this->getServiceLocator()->get("App\Mapper\Statistic");
        $statistic = $statisticMapper->getStatistics($selected);
        $data = $statisticMapper->getData($selected);

    	$this->script()->addTitle("Control Panel Home");
		$this->layout()->messages = $this->flashMessenger()->getMessages();
		$this->ExtjsManager()
    			->setTheme("neptune")
    			->addJs("/js/cp/stats.js")
    			->apply();
		return array(
            'statistic' => $statistic,
            'data' => $data,
            'selected' => $selected
		);
    }
}
