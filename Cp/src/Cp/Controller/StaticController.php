<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\News as NewsModel;

class StaticController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->script()->addJs("/ckeditor/ckeditor.js");
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addNamespace("CustomLib.editor.Ckeditor")
    			->addJs("/js/cp/static-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\StaticPage')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("id", "key", "title", "body"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$form = new \App\Form\StaticPage();
    	$mapper = $sl->get("App\Mapper\StaticPage");
    	
    	$data = $request->getPost();
    	
    	$form->setData($data);
    	$form->setInputFilter($mapper->getInputFilter());
    	
    	$responseData = array();
    	
    	if($form->isValid())
    	{
    		$validData = $form->getData();
    		if($validData['id'] > 0)
    		{
    			$model = $mapper->fetchOne($validData['id']);
    			$mapper->hydrate($validData, $model);
    		    $mapper->save($model);
    		}
    		
    	}	
    	else{
    		$msg = array();
    		$messages = $form->getMessages();
    		foreach($messages as $el)
    		{
    			foreach($el as $message)
    			{
    				$msg[] = $message;
    			}
    		}
    		$responseData['success'] = false;
    		$responseData['msg'] = implode("<br>", $msg);
    		
    	}
    	
    	return $this->response($responseData);
    }
}