<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container as SessionContainer;
use Zend\Json\Json as Json;

class UserController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addNamespace("CustomLib.data.AjaxStore")
    			->addNamespace("CustomLib.grid.ComboFilter")
    			->addJs("/js/cp/users-list.js")
    			->addJs("/js/cp/users-roles.js")
    			->apply();
    	return array(

		);
    }

    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	/* die($this->getServiceLocator()
    			->get('App\Mapper\User')->getDataGridSelect()->getSqlString($db->getPlatform())); */
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\User')->getDataGridSelect($request->getQuery("countryId"), $request->getQuery("cityId")),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("users.id", "cmp.title", "email", "name", "business_name", "users.title", "telephone", "company_registration_number", "users.type_of_seller", 'users.credits'))
    			->getData();
    	 //var_dump($data);
		$roleMapper = $this->getServiceLocator()->get("App\Mapper\Role");

    	foreach($data['data'] as $k=>$v)
    	{
    		$data['data'][$k]['roles'] = implode(",", $roleMapper->getListByUser($v['id']));
    	}

    	return $this->response($data);
    }

    public function roleslistAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\Role')->getDataGridSelect(),
    			$db,
    			null)
    			->getData();
    	return $this->response($data);
    }

	public function updaterolesAction()
	{
    	$request = $this->getRequest();
    	$userId = $request->getPost("user_id");
    	$roles = $request->getPost("roles");
    	$roleMapper = $this->getServiceLocator()->get("App\Mapper\Role");

    	$authService = $this->getServiceLocator()->get('zfcuser_auth_service');
		$user = $authService->getIdentity();

		if($user->getId() == $userId && strstr($roles, "admin") === false)
		{
			//This is a condition for stupid administrators, who want to remove their privileges
			// Sergey Kornienko said me, that just stupid administrators can remove their privileges
			// So I do not let them do it
			return $this->response(array(
				'success' => false,
				'message' => "Hey! Do you wish to remove your privileges? It is impossible to do!!!"
			));
		}
		else
		{
			$roleMapper->updateRoles($userId, ($roles ? explode(",", $roles) : array()));
    	}


    	return $this->response(array(
    		'message' => sprintf(_("You have successfully updated roles for the user #%s"), $userId)
    	));
    }


    public function activateAction()
    {
    	$userId = $this->getRequest()->getPost("user_id");
    	$userMapper = $this->getServiceLocator()->get("App\Mapper\User");
    	$user = $userMapper->fetchOne($userId);
    	$user->setIsActive(1);
    	$userMapper->save($user, array('is_active'));

    	return $this->response(array(
    		'message' => sprintf(_("User #%s has been successfully activated."), $userId)
    	));
    }

    public function deleteAction()
    {
        $userId = $this->getRequest()->getPost("id");
        $userMapper = $this->getServiceLocator()->get("App\Mapper\User");

        $userMapper->deleteUser($userId);

        return $this->response(array(
            'message' => sprintf(_("User #%s has been successfully deleted."), $userId)
        ));
    }

    public function blockAction()
    {
    	$userId = $this->getRequest()->getPost("user_id");
    	$userMapper = $this->getServiceLocator()->get("App\Mapper\User");
    	$user = $userMapper->fetchOne($userId);
    	$user->setIsActive(0);
    	$userMapper->save($user, array('is_active'));

    	return $this->response(array(
    			'message' => sprintf(_("User #%s has been successfully blocked."), $userId)
    	));
    }

    public function downloadsubscribedAction()
    {
    	ini_set('memory_limit', '512M');
		$fname = "subscribed-users-".date("Y-m-d").".csv";
		$path = getcwd() . "/data/".$fname;

		set_time_limit(3000);

		header( 'Content-Type: text/csv' );
		header("Content-Disposition: attachment;filename=".$fname);

		$df = fopen($path, 'w');
		fputcsv($df, array(
			"Email", "Business Name", "Title", "Name", "Type of Seller", "Telephone", "Business Registration Number", "Business Web Address"
		));

		$userMapper = $this->getServiceLocator()->get("App\Mapper\User");
		$list = $userMapper->fetchAll("is_active=1 AND is_subscribed=1");
		foreach($list as $user){
		   fputcsv($df, array(
			   $user->getEmail(),
			   $user->getBusinessName(),
			   $user->getTitle(),
			   $user->getName(),
			   $user->getTypeOfSeller(),
			   $user->getTelephone(),
			   $user->getCompanyRegistrationNumber(),
			   $user->getCompanyRegistrationNumber()
		   ));
		}
		fclose($df);
		readfile($path);
		exit;
    }

    public function loginasAction()
    {
    	$id = $this->getRequest()->getQuery("id");
    	if(!$id)
    	{
    		throw new \Exception("Undefined user ID");
    	}
    	$sl = $this->getServiceLocator();
    	$userMapper = $sl->get("App\Mapper\User");

    	$user = $userMapper->fetchOne($id);
    	if(!$user)
    	{
    		throw new \Exception("Wrong User ID");
    	}

    	$authService = $sl->get("zfcuser_auth_service");

    	$admin = $authService->getIdentity();
	    $this->zfcUserAuthentication()->getAuthService()->clearIdentity();
    	$session = new SessionContainer();

    	if($userMapper->autoLogin($user->getEmail()) == false)
    	{
    		throw new \Exception("You can not log in as user " . $id);
    	}
    	$session->administrator = $admin;

    	//UserActivity
		$userActivityMapper = $this->getServiceLocator()->get("App\Mapper\UserActivity");
		$userMapper = $this->getServiceLocator()->get("App\Mapper\User");
		$data = array(
			"User Data" => $userMapper->extract($user)
		);
		$userActivityMapper->add("general", $admin->getId(), _("Administrator has logged in as another user."), $data);

		if($user->getCountryId() > 0)
		{
			$countryMapper = $this->getServiceLocator()->get("App\Mapper\Country");
			$country = $countryMapper->fetchOne($user->getCountryId());
			if(($code = $country->getInternet()) && $code != '--')
			{
				$locationManager = $this->getServiceLocator()->get("LocationManager");
				if($locationManager->setLocation($code) == true)
				{
					if(($cookieHeader = $locationManager->getCookieHeader()))
					{
						$this->getResponse()->getHeaders()->addHeader($cookieHeader);
					}
				}
			}
		}
		//------

    	$this->redirect()->toRoute("main");
    }

    public function saveAction(){
        $json = $this->getRequest()->getContent();
        $data = Json::decode($json, Json::TYPE_ARRAY);

        $userId = $data["id"];
        $userMapper = $this->getServiceLocator()->get("App\Mapper\User");
        $user = $userMapper->fetchOne($userId);
        $user->setXDaysAdLive($data['x_days_ad_live']);
        $user->setCredits($data['credits']);
        $userMapper->save($user);

        return $this->response(array(
            'message' => sprintf(_("User #%s has been successfully updated."), $userId)
        ));
    }

  /*   public function userroleslistAction(){
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\Role')->getDataGridSelect($request->getQuery("user_id")),
    			$db,
    			null)
    			->getData();
    	return $this->response($data);
    } */
}
