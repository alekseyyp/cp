<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\Faq as FaqModel;

class FaqController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->script()->addJs("/ckeditor/ckeditor.js");
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addNamespace("CustomLib.editor.Ckeditor")
    			->addJs("/js/cp/faq-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\Faq')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("id", "question", "answer"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$form = $sl->get("FaqForm");
    	$mapper = $sl->get("App\Mapper\Faq");
    	
    	$data = $request->getPost();
    	
    	$form->setData($data);
    	$form->setInputFilter($mapper->getInputFilter());
    	
    	$responseData = array();
    	
    	if($form->isValid())
    	{
    		$validData = $form->getData();
    		if($validData['id'] > 0)
    		{
    			$model = $mapper->fetchOne($validData['id']);
    		}
    		else
    		{
    			$model = new FaqModel();
    			$model->setAsks(0)
    					->setIsActive(1)
    					->setViews(0);
    		}	 
    		$model = $mapper->hydrate($validData, $model);
    		
    		
    		$mapper->save($model);
    		
    	}	
    	else{
    		$msg = array();
    		$messages = $form->getMessages();
    		foreach($messages as $el)
    		{
    			foreach($el as $message)
    			{
    				$msg[] = $message;
    			}
    		}
    		$responseData['success'] = false;
    		$responseData['msg'] = implode("<br>", $msg);
    		
    	}
    	
    	return $this->response($responseData);
    }
    
    public function publishAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\Faq");
    	$model = $mapper->fetchOne($id);
    	$model->setIsActive(1);
    	$mapper->save($model, array("is_active"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("The question #%s has been successfully published."), $id)
    	));
    }
    
 	public function deleteAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\Faq");
    	$mapper->delete($id);
    	
    	return $this->response(array(
    		'message' => sprintf(_("The question #%s has been successfully deleted."), $id)
    	));
    }
    
    public function unpublishAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\Faq");
    	$model = $mapper->fetchOne($id);
    	$model->setIsActive(0);
    	$mapper->save($model, array("is_active"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("The question #%s has been successfully unpublished."), $id)
    	));
    }
}
