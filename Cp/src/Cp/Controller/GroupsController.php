<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\SellerGroup as SellerGroupModel;
use App\Form\SellerGroup;

class GroupsController extends AbstractActionController
{
	const SESSION_KEY_GROUPS = "new_group";
	
    public function indexAction()
    {
    	$this->script()->addJs("/ckeditor/ckeditor.js");
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addNamespace("CustomLib.data.AjaxStore")
    			->addNamespace("CustomLib.grid.ComboFilter")
    			->addJs("/js/cp/groups-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
		$mapper = $this->getServiceLocator()->get("App\Mapper\SellerGroup");
    	$data = $this->grid()
    	->initGrid($mapper->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("id", "name"))
    			->getData();
    	return $this->response($data);
    }
    
    public function userslistAction()
    {
	   	$request = $this->getRequest();
	   	$grid = $request->getQuery("grid");
	    $userMapper = $this->getServiceLocator()->get('App\Mapper\User');
	   	$users = $request->getQuery("users");
		
	   	if($grid == 1)
	   	{
	   		$groupUsers = array();
			if($users != null)
			{
				$groupUsers = $userMapper->getAllByIds(explode(",", $users));
			}
			$data = array(
				'data' => $groupUsers,
				'total' => count($groupUsers)
			);	
	   	}
	   	else 
	   	{
	   		//This is a list of all available users
	   		$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
	    	$data = $this->grid()
	    	->initGrid($userMapper->getDataGridSelect($request->getQuery("country_id"), null, ($users ? explode(",", $users) : null)),
	    			$db,
	    			$this->params()->fromQuery())
	    			->setSearchColumns(array("users.id", "cmp.title", "email", "name", "business_name", "users.title", "telephone", "company_registration_number", "users.type_of_seller"))
	    			->getData();
	   	}
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$mapper = $sl->get("App\Mapper\SellerGroup");
    	
    	$form = $sl->get("App\Form\SellerGroup");
    	
    	$data = $request->getPost();
    	
    	$form->setData($data);
    	
    	$form->setInputFilter($mapper->getInputFilter());
    	
    	$responseData = array();
    	
    	if($form->isValid())
    	{
    		$validData = $form->getData();
    		if($validData['id'] > 0)
    		{
    			$model = $mapper->fetchOne($validData['id']);
    		}
    		else
    		{
    			$model = new SellerGroupModel();
    		}	 
    		$model = $mapper->hydrate($validData, $model);
    		
    		
    		$mapper->save($model);
    		
    	}	
    	else{
    		$msg = array();
    		$messages = $form->getMessages();
    		foreach($messages as $el)
    		{
    			foreach($el as $message)
    			{
    				$msg[] = $message;
    			}
    		}
    		$responseData['success'] = false;
    		$responseData['msg'] = implode("<br>", $msg);
    		
    	}
    	
    	return $this->response($responseData);
    }
    
  	public function deleteAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\SellerGroup");
    	
    	$mapper->delete($id);
    	
    	return $this->response(array(
    		'message' => sprintf(_("The group #%s has been successfully deleted."), $id)
    	));
    }
}
