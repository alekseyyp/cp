<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\Subscription as SubscriptionModel;

class SubscriptionController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.data.AjaxStore")
    			->addNamespace("CustomLib.form.field.ComboBox")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addJs("/js/cp/subscriptions-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\Subscription')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("subscriptions.id", "u.name", "u.email", "u.business_name", "from_date", "to_date"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$form = $sl->get("SubscriptionForm");
    	$mapper = $sl->get("App\Mapper\Subscription");

        $userMapper = $this->getServiceLocator()->get('App\Mapper\User');
        $countryMapper = $this->getServiceLocator()->get('App\Mapper\Country');


    	
    	$data = $request->getPost();
    	
    	$form->setData($data);
    	$form->setInputFilter($mapper->getInputFilter());
    	
    	$responseData = array();
    	
    	if($form->isValid())
    	{
    		$validData = $form->getData();
    		if($validData['id'] > 0)
    		{
    			$model = $mapper->fetchOne($validData['id']);
    			$model = $mapper->hydrate($validData, $model);
                $mapper->save($model);
    		}
    		else
    		{
    			$model = new SubscriptionModel();
    			$model = $mapper->hydrate($validData, $model);
                $user = $userMapper->fetchOne($model->getUserId());

                $country = $countryMapper->fetchOne($user->getCountryId());
                $code = $country ? $country->getCurrencyCode() : 'GBP';

    			$model->setCurrency(in_array($code, array('USD', 'EUR', 'GBP')) ? $code : "GBP");
                $model->setCreated(date("Y-m-d H:i:s"));

                $mapper->save($model);

                if ($model->getAmount() == 0) {
                    // instant credits charging
                    try {
                        $mapper->markPaidAndCharge($model->getId());
                    } catch (\Exception $e) {
                        $responseData['success'] = false;
                        $responseData['msg'] = implode("<br>", array($e->getMessage()));
                    }

                }
    		}
    	}
    	else
    	{
    		$msg = array();
    		$messages = $form->getMessages();
    		foreach($messages as $el)
    		{
    			foreach($el as $message)
    			{
    				$msg[] = $message;
    			}
    		}
    		$responseData['success'] = false;
    		$responseData['msg'] = implode("<br>", $msg);
    		
    	}
    	
    	return $this->response($responseData);
    }
    
    public function getusersAction()
    {
    	$query = $this->getRequest()->getQuery("query");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\User");
    	$list = $mapper->getAllAssoc($query);
    	
    	return $this->response(array(
    		'data' => $list,
    		'total'=> count($list)
    	));
    }
    
    public function deleteAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\Subscription");
		$mapper->delete($id);    	
    	return $this->response(array(
    		'message' => sprintf(_("The subscription #%s has been successfully deleted."), $id)
    	));
    }
}
