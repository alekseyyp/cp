<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\Component as ComponentModel;

class ComponentController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->script()->addJs("/ckeditor/ckeditor.js");
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addJs("/js/cp/component-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	
    	$live 		= $request->getQuery("live", 0);
    	$expired	= $request->getQuery("expired", 0);
    	$removed 	= $request->getQuery("removed", 0);
    	$sold 		= $request->getQuery("sold", 0);
    	$notpaid 	= $request->getQuery("notpaid", 0);
    	$mapper = $this->getServiceLocator()
    			->get('App\Mapper\Component');
    	
    	$mapper->setFilter(
    		array(
				'live' => $live,
				'expired' => $expired,
				'removed' => $removed,
				'sold' => $sold,
    			'notpaid' => $notpaid
			)
    	);
    	$data = $this->grid()
    	->initGrid($mapper->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("components.id", "manufacture", "part_number", "type_of_seller", "contact_name", "contact_email", "date_added", "date_published"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$mapper = $sl->get("App\Mapper\Component");
    	
    	$data = $request->getPost()->toArray();
    	
    	$responseData = array();
    	
    	if($data['id'] > 0)
	    {
	    	$model = $mapper->fetchOne($data['id']);
	    	$data['photo1'] = $model->getPhoto1();
	    	$data['photo2'] = $model->getPhoto2();
	    	$data['photo3'] = $model->getPhoto3();
	    	$data['photo4'] = $model->getPhoto4();
	    	$model = $mapper->hydrate($data, $model);
	    }
	    else
	    {
	    	$model = new ComponentModel();
	    	$model = $mapper->hydrate($data, $model);
	    	$model->setIsActive(1)
	    			->setCurrency('GBP')
	    			->setIsSold(0)
					->setIsRemoved(0)
					->setIsExpired(0)
	    	        ->setIsOrdered(0)
	    	        ->setShowName(0)
	    	        ->setShowPhone(0)
	    	        ->setSecureEmail(0);
	   	}	 
	   	$rData = $request->getFiles()->toArray();
    	if(!empty($rData["photo1"]) && !empty($rData["photo1"]['name']))
    	{
			$fileName = $mapper->saveFile($rData["photo1"]);
			$model->setPhoto1($fileName);
		}
    	if(!empty($rData["photo2"]) && !empty($rData["photo2"]['name']))
    	{
			$fileName = $mapper->saveFile($rData["photo2"]);
			$model->setPhoto2($fileName);
		}
        
        if(!empty($rData["photo3"]) && !empty($rData["photo3"]['name']))
    	{
			$fileName = $mapper->saveFile($rData["photo3"]);
			$model->setPhoto3($fileName);
		}

        if(!empty($rData["photo4"]) && !empty($rData["photo4"]['name']))
    	{
			$fileName = $mapper->saveFile($rData["photo4"]);
			$model->setPhoto4($fileName);
		}
		
		$mapper->save($model);
		
    	return $this->response($responseData);
    }
    
  	public function deleteAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\Component");
    	$component = $mapper->fetchOne($id);
    	
    	$component->setIsRemoved(1);
    	
    	$mapper->save($component, array("is_removed"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("The ad #%s has been successfully removed."), $id)
    	));
    }
}
