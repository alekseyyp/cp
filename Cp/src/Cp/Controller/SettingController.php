<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\Setting as SettingModel;
use Zend\Json\Json as Json;

class SettingController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addJs("/js/cp/settings-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\Setting')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$form = $sl->get("SettingForm");
    	$mapper = $sl->get("App\Mapper\Setting");
    	
    	$json = $request->getContent();
    	$data = Json::decode($json, Json::TYPE_ARRAY);
    	$data['value'] = (string)$data['value'];
    	$form->setData($data);
    	$form->setInputFilter($mapper->getInputFilter());
    	
    	$responseData = array();
    	
    	if($form->isValid())
    	{
    		$validData = $form->getData();
    		if($validData['id'] > 0)
    		{
    			$model = $mapper->fetchOne($validData['id']);
    			$model = $mapper->hydrate($validData, $model);
    			$mapper->save($model);
    			$responseData['msg'] = sprintf(_("The parameter %s has been successfully updated."), $model->getKey());
    		}
    	}else{
    		//TODO Something wrong
    		var_dump($form->getMessages());
    	}
    	
    	return $this->response($responseData);
    }
}
