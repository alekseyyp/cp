<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\WantedComponent as WantedComponentModel;
use App\Form\WantedComponent;

class WantedcomponentController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->script()->addJs("/ckeditor/ckeditor.js");
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addNamespace("CustomLib.data.AjaxStore")
    			->addNamespace("CustomLib.form.field.ComboBox")
    			->addJs("/js/cp/wantedcomponent-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	
    	$live 		= $request->getQuery("live", 0);
    	$expired	= $request->getQuery("expired", 0);
    	$removed 	= $request->getQuery("removed", 0);
    	$found 		= $request->getQuery("found", 0);
    	$notpaid 	= $request->getQuery("notpaid", 0);
    	
    	$mapper = $this->getServiceLocator()
    			->get('App\Mapper\WantedComponent');
    	
    	$mapper->setFilter(
    		array(
				'live' 		=> $live,
				'expired' 	=> $expired,
				'removed' 	=> $removed,
				'found' 	=> $found,
    			'notpaid' 	=> $notpaid
    		)
    	);
    	$data = $this->grid()
    	->initGrid($mapper->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("wanted_components.id", "manufacture", "part_number", "contact_name", "contact_email", "date_added", "date_published"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$mapper = $sl->get("App\Mapper\WantedComponent");
    	
    	$data = $request->getPost()->toArray();
    	
    	$responseData = array();
    	$logger = $sl->get('Logger');
    	$logger->setClass(__CLASS__);
    	
    	if($data['id'] > 0)
	    {
    		$logger->debug(var_export($data, 1));
	    	$model = $mapper->fetchOne($data['id']);
	    	$model = $mapper->hydrate($data, $model);
	    }
	    else
	    {
	    	$model = new WantedComponentModel();
	    	$model = $mapper->hydrate($data, $model);
	    	$model->setIsActive(1)
	    			->setIsFound(0)
					->setIsRemoved(0)
					->setIsExpired(0)
	    			->setCondition("ANY")
	    			->setIsOrdered(0);
	   	}	 
		$mapper->save($model);
		
    	return $this->response($responseData);
    }
    
  	public function deleteAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\WantedComponent");
    	$component = $mapper->fetchOne($id);
    	
    	$component->setIsRemoved(1);
    	
    	$mapper->save($component, array("is_removed"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("The post #%s has been successfully removed."), $id)
    	));
    }
    
    public function getgroupsAction()
    {
    	$query = $this->getRequest()->getQuery("query");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\SellerGroup");
    	$list = $mapper->getAllAssoc($query);
    	
    	return $this->response(array(
    		'data' => $list,
    		'total'=> count($list)
    	));
    }
    
    public function forwardAction()
    {
    	$request = $this->getRequest();
    	$sl = $this->getServiceLocator();
    	$postQueueMapper = $sl->get("App\Mapper\PostQueue");
    	$msg = null;
    	if($request->isPost())
    	{
    		$sellerGroupMapper = $sl->get("App\Mapper\SellerGroup");
    		$wantedComponentMapper = $sl->get("App\Mapper\WantedComponent");
    		
    		$group_id 	= $request->getPost("group_id");
    		$ads = $strIds	= $request->getPost("ads");
    		$message	= $request->getPost("message");
    		
    		$sellerGroup = $sellerGroupMapper->fetchOne($group_id);
    		$ads = explode(",", $ads);
    		foreach($ads as $adId)
    		{
    			$adId = trim($adId);
    			if($adId > 0)
    			{
	    			$wantedComponenet = $wantedComponentMapper->fetchOne($adId);
	    			$postQueueMapper->addToQueue($sellerGroup, $wantedComponenet, $message);
    			}
    		}
    		$msg = "Posts " . $strIds . " have been added to the queue. Posts will be sent to sellers as soon as possible.";
    	}
    	return $this->response(array(
    		'msg' => $msg
    	));
    }
}
