<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;

use App\Model\Order as OrderModel; 

class OrdersController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addJs("/js/cp/orders-list.js")
    			->apply();
    	
    	$this->script()->addTitle("Payment Log");
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$orderMapper = $this->getServiceLocator()->get('App\Mapper\Order');
    	$componentMapper = $this->getServiceLocator()->get('App\Mapper\Component');
    	$wantedComponentMapper = $this->getServiceLocator()->get('App\Mapper\WantedComponent');
    	$paid = $request->getQuery("paid", 1);
    	$data = $this->grid()
    	->initGrid($orderMapper->getDataGridSelect($paid),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("orders.id", "u.name", "user_id", "created", "paid"))
    			->getData();
    	 foreach ($data['data'] as $k=>$v) {
    	     $tmp = @unserialize($v['items']);
    	     if ($tmp) {
        	     
    	         $items = array();
        	     foreach ($tmp as $val) {
        	     	
        	     	if(isset($val['type'])) {
        	     		
	        	     	if($val['type'] == 'SELL') {
	            	        
	        	     	    $component = $componentMapper->fetchOne($val['adId']);
	            	     	if($component) {
	                	     	$items[] = array(
	                	     		'id' => $component->getId(),
	                	     	    'title' => $component->getTitle(),
	                	     	    'manufacture' => $component->getManufacture(),
	                	     	    'part_number' => $component->getPartNumber()
	                	     	);
	            	     	}
	            	     	
	        	     	} elseif ($val['type'] == 'WANT') {
	
	        	     	    $component = $wantedComponentMapper->fetchOne($val['adId']);
	        	     	    if($component) {
	        	     	        $items[] = array(
	        	     	            'id' => $component->getId(),
	        	     	            'title' => $component->getTitle(),
	        	     	            'manufacture' => $component->getManufacture(),
	        	     	            'part_number' => $component->getPartNumber()
	        	     	        );
	        	     	    }
	        	     	    
	        	     	}
	        	     	
        	     	}
        	     }
        	     
    	     } else {
    	     	
    	         $items = array();
    	         
    	     }
    	     $order = new OrderModel();
    	     $order = $orderMapper->hydrate($v, $order);
    	     if($order->getVat()) {
    	     	
    	     	 $data['data'][$k]['vat'] = $order->toPay(true, false) * $order->getVat() / 100;
    	     	
    	     }
    	     $data['data'][$k]['total'] = $order->toPay();
    	     $data['data'][$k]['items'] = var_export($items, 1);
    	     
    	     $tmp = @unserialize($v['user']);
    	     
    	     if ($tmp) {
    	         
    	         $data['data'][$k]['user'] = var_export(array(
        	     	 'id' => $tmp['id'],
        	         'name' => $tmp['name'],
        	         'email' => $tmp['email'],
        	         'business_name' => $tmp['business_name']
        	     ), 1);

    	     } else {
    	         $data['data'][$k]['user'] = "";
    	     }
    	     
    	 }
    	return $this->response($data);
    }
    
    public function downloadAction()
    {
    	$paid = $this->getRequest()->getQuery('paid', 1);
    	ini_set('memory_limit', '512M');
    	$fname = "payment-log-". ($paid == 1 ? '-paid' : '-non-paid') . date("Y-m-d").".csv";
    	$path = getcwd() . "/data/".$fname;
    
    	set_time_limit(3000);
    
    	header('Content-Type: text/csv' );
    	header("Content-Disposition: attachment;filename=".$fname);
    
    	$df = fopen($path, 'w');
    	fputcsv($df, array(
    		"User ID", "Business Name", "VAT Number", "CP Invoice No", "Sub total", "VAT", 
    		"Applied Credits", "Applied Coupons", "Total", "Currency", "Created Date", "Paid date"
    	));
    	/* @var $orderMapper \App\Mapper\Order */
    	$orderMapper = $this->getServiceLocator()->get("App\Mapper\Order");
    	$results = $orderMapper->getCsvData($paid);
   		foreach($results as $row){
    	
   			$order = new \App\Model\Order();
   			$order = $orderMapper->hydrate($row, $order);
   			$toPay = $order->toPay(true, false);
   			if(($vat = $order->getVat()) > 0) {
   				
   				$vat = $toPay * $vat / 100;
   				
   			}
   			$appliedCoupons = 0;
   			if (count($order->coupons())) {
   				
   				$appliedCoupons = $order->toPay(false, false) - $toPay;
   				
   			}
   			
			fputcsv($df, array(
	    		$row['user_id'],
	    		($row['business_name'] ? $row['business_name'] : $row['name']),
	    		$row['vat_number'],
	    		$order->getId(),
	    		$row['subtotal'],
	    		$vat,
	    		$order->getAppliedCredits(),
	    		$appliedCoupons,
	    		$order->toPay(),
	    		$order->getCurrency(),
	    		$order->getCreated(),
	    		$order->getPaid()
    		));
    	}
    	fclose($df);
    	readfile($path);
    	exit;
    }
}
