<?php
namespace Cp\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use App\Model\Ad as AdModel;

class AdController extends AbstractActionController
{
    public function indexAction()
    {
    	$this->ExtjsManager()
    			->setTheme("neptune")
    			->addNamespace("CustomLib.grid.SearchField")
    			->addNamespace("CustomLib.window.Message")
    			->addNamespace("CustomLib.manage.Base")
    			->addNamespace("CustomLib.window.WindowForm")
    			->addNamespace("CustomLib.data.AjaxStore")
    			->addNamespace("CustomLib.form.field.ComboBox")
    			->addLanguageVar("blankZoneText", " -- Choose a Zone --")
    			->addJs("/js/cp/ads-management.js")
    			->apply();
    	return array(
				
		);
    }
    
    public function listAction()
    {
    	$request = $this->getRequest();
    	$db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	$data = $this->grid()
    	->initGrid($this->getServiceLocator()
    			->get('App\Mapper\Ad')->getDataGridSelect(),
    			$db,
    			$this->params()->fromQuery())
    			->setSearchColumns(array("ads.id", "az.zone", "alt", "code", "url", "type"))
    			->getData();
    	return $this->response($data);
    }
    
    public function saveAction()
    {
    	$request = $this->getRequest();
    	
    	$sl = $this->getServiceLocator();
    	
    	$form = $sl->get("AdForm");
    	$mapper = $sl->get("App\Mapper\Ad");
    	
    	$data = $request->getPost()->toArray();
    	$form->setData($data);
    	$form->setInputFilter($mapper->getInputFilter());
    	
    	$responseData = array();
    	
    	if($form->isValid())
    	{
    		$validData = $form->getData();
    		$success = true;
    		if($validData['type'] == 'Image')
    		{
    			if($validData['id'] > 0)
	    		{
	    			$model = $mapper->fetchOne($validData['id']);
	    			$validData['picture'] = $model->getPicture();
	    			$model = $mapper->hydrate($validData, $model);
	    		}
	    		else
	    		{
	    			$model = new AdModel();
	    			$model = $mapper->hydrate($validData, $model);
	    			$model->setIsActive(1)
	    					->setViews(0)
	    					->setClicks(0);
	    		}	 
	    		$rData = $request->getFiles()->toArray();
    			$pictureFileData = $rData["picture"];
    			if(!empty($pictureFileData) && !empty($pictureFileData['name']))
    			{
    				$fileName = $mapper->saveFile($pictureFileData);
    				
    				$model->setPicture($fileName);
    				$mapper->save($model);
    			}
    			else
    			{
    				if(!$validData['id'])
    				{
	    				$success = false;
	    				$responseData['success'] = false;
	    				$responseData['msg'] = _("Picture file should be selected.");
    				}
    				else
    				{
    					$mapper->save($model);
    				}
    			}
    		}
    		else
    		{
	    		if($validData['id'] > 0)
	    		{
	    			$model = $mapper->fetchOne($validData['id']);
	    			$model = $mapper->hydrate($validData, $model);
	    		}
	    		else
	    		{
	    			$model = new AdModel();
	    			$model = $mapper->hydrate($validData, $model);
	    			$model->setIsActive(1)
	    					->setViews(0)
	    					->setClicks(0);
	    		}	 
	    		
	    		
	    		$mapper->save($model);
    		}
    		
    	}	
    	else{
    		$msg = array();
    		$messages = $form->getMessages();
    		foreach($messages as $el)
    		{
    			foreach($el as $message)
    			{
    				$msg[] = $message;
    			}
    		}
    		$responseData['success'] = false;
    		$responseData['msg'] = implode("<br>", $msg);
    		
    	}
    	
    	return $this->response($responseData);
    }
    
    public function activateAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\Ad");
    	$model = $mapper->fetchOne($id);
    	$model->setIsActive(1);
    	$mapper->save($model, array("is_active"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("Ad #%s has been successfully activated."), $id)
    	));
    }
    
	public function getzonesAction()
    {
    	$mapper = $this->getServiceLocator()->get("App\Mapper\AdZone");
    	
    	$list = $mapper->getAllAssoc();
    	
    	return $this->response(array(
    		'data' => $list,
    		'total'=> count($list)
    	));
    }
    
    public function blockAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\Ad");
    	$model = $mapper->fetchOne($id);
    	$model->setIsActive(0);
    	$mapper->save($model, array("is_active"));
    	
    	return $this->response(array(
    		'message' => sprintf(_("Ad #%s has been successfully blocked."), $id)
    	));
    }
    
	public function deleteAction(){
    	$id = $this->getRequest()->getPost("id");
    	$mapper = $this->getServiceLocator()->get("App\Mapper\Ad");
    	$model = $mapper->fetchOne($id);
    	$mapper->deleteAd($model);
    	
    	return $this->response(array(
    		'message' => sprintf(_("Ad #%s has been successfully deleted."), $id)
    	));
    }
}
